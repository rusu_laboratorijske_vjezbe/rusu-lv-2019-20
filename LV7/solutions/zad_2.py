# -*- coding: utf-8 -*-
"""Untitled20.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/16rMPISnccOLnHH8-nMOnk8FcJLFGkujA
"""

from keras.preprocessing.image import img_to_array
from keras.models import load_model
from matplotlib import pyplot as plt
from skimage.transform import resize
from skimage import color
import cv2 as cv
import numpy as np

filename = 'test.png'

img= cv.imread(filename)
#----------------------------------------------------------
loadedImage = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
loadedImage = resize(loadedImage, (28, 28))


plt.figure()
plt.imshow(img, cmap=plt.get_cmap('gray'))
plt.show()


loadedImage = loadedImage.reshape(1, 28, 28, 1)
loadedImage = loadedImage.astype('float32')


# TODO: ucitaj model
model = load_model('test_model.h5')

# TODO: napravi predikciju 
prediction = model.predict(loadedImage)

# TODO: ispis rezultat
print("********************")
print(prediction.argmax())