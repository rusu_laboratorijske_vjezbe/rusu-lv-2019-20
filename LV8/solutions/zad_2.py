# -*- coding: utf-8 -*-
"""Untitled20.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/16rMPISnccOLnHH8-nMOnk8FcJLFGkujA
"""

import pandas as pd
from tensorflow import keras
from tensorflow.keras import layers
from shutil import copy2
import os

testData = pd.read_csv('Test.csv', index_col = 0)
print(testData)
path = os.getcwd()
os.makedirs('Test_Dir', exist_ok=True)
for i in range (43):
    os.makedirs(path + '/Test_Dir/' + str(i), exist_ok=True)
    
    
for _, row in testData.iterrows():
    endpath = row.Path.split('/')
    name = endpath[1]
    copy2(row.Path, path + '/Test_Dir/' + str(row.ClassId) + '/' + name)
    
datasetTest = keras.utils.image_dataset_from_directory(directory='Test_Dir/',
 labels='inferred',
 label_mode='categorical',
 batch_size=32,
 image_size=(48, 48))
datasetTrain = keras.utils.image_dataset_from_directory(directory='Train/',
 labels='inferred',
 label_mode='categorical',
 batch_size=32,
 image_size=(48, 48))


num_classes = 43
input_shape = (48, 48, 3)

modelKeras = keras.Sequential()
modelKeras.add(keras.Input(input_shape))
modelKeras.add(layers.Conv2D(32, kernel_size=(3, 3), activation='relu'))
modelKeras.add(layers.Conv2D(32, (3, 3), activation='relu'))
modelKeras.add(layers.MaxPooling2D(pool_size=(2, 2)))
modelKeras.add(layers.Dropout(0.2))
modelKeras.add(layers.Conv2D(64, (3, 3), activation='relu'))
modelKeras.add(layers.Conv2D(64, (3, 3), activation='relu'))
modelKeras.add(layers.MaxPooling2D(pool_size=(2, 2)))
modelKeras.add(layers.Dropout(0.2))
modelKeras.add(layers.Conv2D(128, (3, 3), activation='relu'))
modelKeras.add(layers.Conv2D(128, (3, 3), activation='relu'))
modelKeras.add(layers.MaxPooling2D(pool_size=(2, 2)))
modelKeras.add(layers.Dropout(0.2))
modelKeras.add(layers.Flatten())
modelKeras.add(layers.Dense(512, activation = 'relu', ))
modelKeras.add(layers.Dropout(0.5))
modelKeras.add(layers.Dense(num_classes,activation = 'softmax'))
modelKeras.summary()

modelKeras.compile(loss='categorical_crossentropy',optimizer='sgd',metrics=['accuracy'])

batch_size = 128
epochs = 4
modelKeras.fit_generator(datasetTrain,epochs=epochs)

scoreTest = modelKeras.evaluate_generator(datasetTest)
print('Sequential Test loss:', scoreTest[0])
print('Sequential Test accuracy:', scoreTest[1])

scoreTrain = modelKeras.evaluate_generator(datasetTrain)
print('Sequential Train loss:', scoreTrain[0])
print('Sequential Train accuracy:', scoreTrain[1])