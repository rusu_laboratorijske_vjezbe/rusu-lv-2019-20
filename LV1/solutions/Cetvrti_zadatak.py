def avg(skupBrojeva):
    return sum(skupBrojeva)/len(skupBrojeva)

i=0
skupBrojeva=[]
while(1):
    unosPodatka=input("Unesi broj: ")
    if(unosPodatka=="Done"):
            break
    try:
        skupBrojeva.append(float(unosPodatka))
    except:
        print("Broj nije unešen")
        continue

print("Količina brojeva: ",len(skupBrojeva)
      ,"\nNajveći broj:",max(skupBrojeva)
      ,"\nNajmanji broj:",min(skupBrojeva)
      ,"\nSrednji broj:",avg(skupBrojeva))
