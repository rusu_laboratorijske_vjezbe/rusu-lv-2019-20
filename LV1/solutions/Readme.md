Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

Umetanje zagrada kod print funkcije,
umjesto raw_input() postavljamo input(),
fname umjesto fnamex
counts[word] += 1 povećavanje broja ponavljanja riječi u slučaju da se riječ ponovi.