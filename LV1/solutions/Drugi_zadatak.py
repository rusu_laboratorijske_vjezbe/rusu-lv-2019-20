def generiranjeOcjene(ocjena):
    slovo='A'
    if(ocjena<0.6):
        slovo = 'F'
    elif(ocjena<0.7):
        slovo = 'D'
    elif(ocjena<0.8):
        slovo = 'C'
    elif(ocjena<0.9):
        slovo = 'B'
    else:
        slovo = 'A'
    return slovo

try:
    ocjena=float(input("Unesi broj iz intervala 0.0 - 0.1: "))
    if((ocjena<0.0) or (ocjena>1.0)):
        print("Unesen broj je izvan intervala")
    else:
        print(ocjena, generiranjeOcjene(ocjena))
except:
    print("Uneseni podatak nije broj")


