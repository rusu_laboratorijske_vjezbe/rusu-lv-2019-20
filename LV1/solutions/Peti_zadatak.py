def avg(skupBrojeva):
    return sum(skupBrojeva)/len(skupBrojeva)

fname = input('Unesi ime datoteke: ')  #e.g. www.py4inf.com/code/romeo.txt
try:
    fhand = open(fname)
except:
    print ('Datoteku nije moguće otvoriti:', fname)
    exit()
pouzdanostSpamFiltra=[]
for line in fhand:
    riječi = line.split()
    
    for idx, riječ in enumerate(riječi):
        if(riječ=="X-DSPAM-Confidence:"):
            pouzdanostSpamFiltra.append(float(riječi[idx+1]))

print("Ime datoteke: ", fname)
print("Srednja vrijednost X-DSPAM-Confidence: ",avg(pouzdanostSpamFiltra))
